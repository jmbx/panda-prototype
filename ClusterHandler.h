#pragma once
#include <vector>
#include "Cluster.h"
#include "Hit.h"

using std::vector;

class ClusterHandler {
	public:
		static void sortHits(vector<Hit> hits, int numHits);
		static void cluster(vector<Cluster> &clusters);
		static vector<Cluster> cluster(Hit *hits, int numHits);
		static vector<Cluster> toClusters(Hit* clusterHits, 
				int numHits, int* clusterIndex, int numClusters);
		static void toAlignedClusters(vector<Cluster> clusters, 
				vector<Hit> clusterHits, vector<int> clusterIndex);
		static void merge(vector<Cluster> &a, vector<Cluster> &b);
	private:
		static vector<Cluster> clusterHits(std::vector<Hit> &hits);
};

