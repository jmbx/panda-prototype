#include "GridPrinter.h"
#include <iostream>
#include <fstream>
#include <cmath>

// Program to print the coordinates of the hexagonal grid representing the STT tubes
void GridPrinter::printGrid() {
	std::ofstream output;
	int sl = Detector::sideLength;
	output.open("grid.txt");
	for (int strawID = 0; strawID < sl*sl-1; strawID++) {
		float x,y;

		if((strawID/sl)%2==0) {
			x = (float)(strawID - sl * (strawID/sl));
		} else {
			x = (float)(strawID - sl * (strawID/sl)) + 0.5;
		}

		y = (float)(strawID/sl) * sqrt(3)/2;
		output << x << " " << y << " "<< strawID << std::endl;
	}
	output.close();
}

