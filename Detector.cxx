

#include "Detector.h"
#include <random>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <tr1/random>

const int Detector::sideLength = 20;
const float Detector::epsilon = 0.1f;
const float Detector::sigma = 0.001;
int Detector::stackSize = 100;
int Detector::numHits = 1000;
bool Detector::verbose = false;
bool Detector::write = true;

std::ofstream output;
//Method for generating a list of hits randomly
void Detector::generateRandHits(std::vector<Hit> &hits, float t0, float tEnd, int numHits) {
	float dt=(tEnd - t0)/numHits; //Time step
	std::tr1::ranlux64_base_01 generator; 
	//std::tr1::default_random_engine generator;
	std::tr1::normal_distribution<double> distribution(0,sigma);

	int numStraws = sideLength * sideLength;
	for (int i=0; i < numHits; i++) {
		Hit h;
		h.time = t0 + i*dt + distribution(generator);
		h.strawID = rand() % numStraws;
		hits.push_back(h);
		//std::cout<<"Hit created, time = "<<hits[i].time<<", strawID = "<<hits[i].strawID<<"."<<std::endl;
	}

}

// Method for generating hits in a line
// Only generates horizontal lines (for now)
void Detector::generateLineHits(Hit* hits) {

	float dt = 0.02; //Time step
	std::tr1::ranlux64_base_01 generator; 
	//std::default_random_engine generator;
	std::tr1::normal_distribution<double> distribution(0,sigma);
	int height = 5;//rand() % (NUMSTRAWS/sideLength);

	for (int i=0; i < Detector::sideLength; i++) {
		hits[i].time = i*dt ;//+ distribution(generator);
		hits[i].strawID = height*Detector::sideLength + i;
	}
}
// Method for generating a line given parameters
void Detector::generateLineHits(std::vector<Hit> &hits, float angle, float xcenter, float ycenter, float t0, int dontAddID) {
	float x = xcenter, y= ycenter;
	float dx = 0.05* std::cos(angle);
	float dy = 0.05 * std::sin(angle);

	std::tr1::ranlux64_base_01 generator; 
	//std::default_random_engine generator;
	std::tr1::normal_distribution<double> distribution(0,sigma);
	float dt = 0.0007;
	float t = t0;
	int ID;
	float rSq;
	int last = -1;
	int lastWritten = -1;
	float lastRSq = 2;
	if(Detector::write) {
		output << x << " " << y << " " << std::endl;
	}
	while (x > -0.25  && y > -0.25 && x < Detector::sideLength && y < sqrt(3)/2 * Detector::sideLength-0.5) {
		
		ID = Hit::pos2ID(x,y);
		rSq = (x-Hit::getPosX(ID))*(x-Hit::getPosX(ID)) + (y-Hit::getPosY(ID))*(y-Hit::getPosY(ID));
		if (ID != lastWritten && ID != -1 && rSq > lastRSq) {
			Hit h; 
			h.strawID = ID;
			h.time = t +std::sqrt(std::sqrt(rSq))*Detector::epsilon*sqrt(2) + 0.01*distribution(generator);
		//	printf("ID %d at (%f,%f), rSq = %f, lastRSq = %f.\n",ID,x,y,rSq, lastRSq);
			if(ID != dontAddID){
				hits.push_back(h);
			}
			t += dt;
			lastWritten = ID;
		}
		if (ID != last) {
			lastRSq = 0.25;
		} else {
			lastRSq = rSq;
		}
		last = ID;
		x += dx;
		y += dy;
	}
	if(Detector::write) {
		output << x << " " << y << " " << std::endl << std::endl;
	}
	
}

//Method for generating a V, i.e. two lines originating from the same s point in spacetime
void Detector::generateV(std::vector<Hit> &hits, float directionAngle, float separationAngle, float xcenter, float ycenter, float t0) {
	int n = (int)hits.size();
	generateLineHits(hits, directionAngle+separationAngle, xcenter, ycenter, t0, -1);
	int dontAddID = hits[n].strawID;
	generateLineHits(hits, directionAngle-separationAngle, xcenter, ycenter, t0, dontAddID);

}

// Method for generating a combination of actual tracks and noise
void Detector::generateHits(std::vector<Hit> &hits) {
	if(Detector::write){
		output.open("line.txt");
	}
	float x,y,angle,sepAngle;
	float t0 = 0;
	//float tEnd = 3*Detector::epsilon;
	float dt = Detector::epsilon;
	while(true) {
		//float p = (float)rand()/(float)(RAND_MAX/a)

		sepAngle = ((double) rand() / (RAND_MAX))*2*M_PI;
		angle = ((double) rand() / (RAND_MAX))*2*M_PI;
		angle = 0;
		sepAngle = M_PI / 10.0;
		x = ((double) rand() / (RAND_MAX)) * sideLength;
		y = ((double) rand() / (RAND_MAX)) * sideLength*sqrt(3)/2;
		x = 5;
		y = 8;
		//t0 = ((double) rand() / (RAND_MAX)) * 1;
		generateV(hits, angle, sepAngle, x, y, t0);
		//generateRandHits(hits, t0, t0 + dt, 20);
		t0 += Detector::epsilon;
		if((int)hits.size() >= numHits) {
			break;
		}
	} 
	hits.resize(numHits);
	if(Detector::write) {
		output.close();
	}
	//generateRandHits(hits, 1, 20, numRandHits);
}

