#include "Timer.h"
#include <iostream>

void Timer::start() {
		t1 = high_resolution_clock::now();
}

void Timer::stop() {
		t2 = high_resolution_clock::now();
}

void Timer::stopAndPrint() {
		stop();
			print();
}

void Timer::stopAndPrint(std::string init) {
		stop();
			std::cout << init;
				print();
}

void Timer::print() {
		auto duration = duration_cast<microseconds>( t2 - t1 ).count();
			std::cout << duration / 1000.0 << std::endl; 
}
