#pragma once

#define EPSILON 0.1
#define SIDELENGTH 10

//Hit struct, contains ID of straw and time of hit
typedef struct {
	int strawID;
	float time;
} Hit;

//Method to check if two hits are considered neighbours (requires neighbouring straws and hit times closer than epsilon)
bool neighbour(Hit a, Hit b){
	int diff=a.strawID - b.strawID;
	if((a.strawID/SIDELENGTH)%2==0) {
		return (std::abs(diff) <= 1 || std::abs(diff) == SIDELENGTH || std::abs(diff-1) == SIDELENGTH) && std::abs(a.time - b.time) < EPSILON; 	
	} else {
		return (std::abs(diff) <= 1 || std::abs(diff) == SIDELENGTH || std::abs(diff+1) == SIDELENGTH) && std::abs(a.time - b.time) < EPSILON; 	
	}
}

//Get method for straw x,y coordinates

float getStrawX(int ID) {

	if((ID/SIDELENGTH)%2==0) {
		return (float)(ID - SIDELENGTH * (ID/SIDELENGTH));
	} else {
		return (float)(ID - SIDELENGTH * (ID/SIDELENGTH)) + 0.5;
	}
}


float getStrawY(int ID) {
		return (float)(ID/SIDELENGTH);
}

//Cluster struct for a chain of hits 
typedef struct {
	std::vector<Hit> hits;
} Cluster;

//Struct for aligned clusters in memory
typedef struct {
	std::vector<Hit> hits;
	std::vector<int> indices;
} AlignedClusters;

//Method to check if two clusters are joinable
bool canJoin(Cluster a, Cluster b){
	for (int i=0; i < (int)a.hits.size(); i++) {
		for(int j=0; j < (int)b.hits.size(); j++) {
			if (neighbour(a.hits[i],b.hits[j])) {
				return true;
			}
		}
	}

	return false;
}

std::vector<Cluster> cluster(std::vector<Cluster> clusters){
	bool changed;
merged: changed = true;
		while(changed){
			changed = false;
			for(int i = 0; i < (int)clusters.size(); i++){
				for(int j = 0; j < (int)clusters.size(); j++){
					if(i!=j && canJoin(clusters[i], clusters[j])){
						clusters[i].hits.insert(clusters[i].hits.end(), clusters[j].hits.begin(), clusters[j].hits.end()); 
						clusters.erase(clusters.begin() + j);
						goto merged;	
					}
				}	
			}	
		}
		//Printing clustering result
		//printf("Clustering result\n");
		for (int i = 0; i < (int)clusters.size(); i++) {
			//printf("Cluster# %d : ", i);
			for (int j=0; j < (int)clusters[i].hits.size(); j++) {
				//printf("(%f, %d)", clusters[i].hits[j].time, clusters[i].hits[j].strawID);
			}
			//printf("\n");
		}
		return clusters;
}

//Method to merge a series of events to a cluster
std::vector<Cluster> cluster(Hit *hits, int numHits){

	std::vector<Cluster> clusters;
	for(int i = 0; i < numHits; i++){
		Cluster cl;
		cl.hits.push_back(hits[i]);
		clusters.push_back(cl);
	}
	return cluster(clusters);
}

std::vector<Cluster> toClusters(Hit* clusterHits, int numHits, int* clusterIndex, int numClusters){
	std::vector<Cluster> newClusters;
	int start = 0;
	for(int j = 0; j < numClusters; j++){
		Cluster cl;
		std::vector<Hit> newHits;

		for(int k = start; k < clusterIndex[j]; k++){
			Hit h;
			h.time = clusterHits[k].time;
			h.strawID = clusterHits[k].strawID;
			newHits.push_back(h);	
		}
		cl.hits.insert(cl.hits.begin(), newHits.begin(), newHits.end());
		newClusters.push_back(cl);	
		start = clusterIndex[j];
	}
	return newClusters;
}

void toAlignedClusters(std::vector<Cluster> clusters, std::vector<Hit> clusterHits, std::vector<int> clusterIndex) {

	for(int i = 0; i < (int)clusters.size(); i++){
		clusterHits.insert(clusterHits.end(), clusters[i].hits.begin(), clusters[i].hits.end()); 
		clusterIndex.push_back(clusterHits.size());
	}	
}

