/*
 * =====================================================================================
 *
 *       Filename:  test.cxx
 *
 *    Description:  Testfile for computational project
 *
 *        Version:  1.0
 *        Created:  2016-11-08 12:12:06
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Björn Andersson (), bjorn.aaa@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include "Hit.h"
#include "Detector.h"
#include "Cluster.h"
#include <vector>
#include <stdio.h>
#include <cmath>
#include "TrackFinder.h"
#include <iostream>
#include <chrono>
#include "ClusterHandler.h"
#include "GridPrinter.h"
#include <fstream>
#include <iomanip> 

using namespace std::chrono;

void writeHitsToFile(std::vector<Cluster> clusters, int n) {
	std::ofstream myfile;
	myfile.open ("hits.txt");

	for(int i = 0; i < (int)clusters.size(); i++){
		for(int j = 0; j < (int)clusters[i].hits.size(); j++){
			float x = clusters[i].hits[j].getPosX();
			float y = clusters[i].hits[j].getPosY();
			myfile << x << " " <<  y << " " <<
				std::fixed << std::setprecision (2) << clusters[i].hits[j].time << " " << i << std::endl;
		}
	}
	myfile.close();
}

int main(int argc, char* argv[]) {


	std::vector<Cluster> cs;
	TrackFinder tf(1);
	std::vector<Hit> hits;
	Detector::generateHits(hits);
	
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	std::vector<Cluster> clusters = ClusterHandler::cluster(&hits.front(), hits.size());
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>( t2 - t1 ).count();
//	std::cout << duration / 1000.0 << std::endl; 
	

	long time = 0;
	for(int i = 0; i < (int)clusters.size() && i < 1; i++) {
		high_resolution_clock::time_point t1 = high_resolution_clock::now();
		if(clusters[i].hits.size() > 5) {
			tf.generateTracks(clusters[i].hits);
		}
		high_resolution_clock::time_point t2 = high_resolution_clock::now();
		auto duration = duration_cast<microseconds>( t2 - t1 ).count();
		time += duration;
		//std::cout << "-------------------------" << std::endl;
	} 
	std::cout << "Time (ms) " << time << std::endl; 
	writeHitsToFile(clusters, 0); 
	GridPrinter::printGrid();

	return 0;
}


