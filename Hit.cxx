#include "Hit.h"
#include "Detector.h"
#include <cmath>
#include <stdio.h>

// Method to check if two strawIDs correspond to neighbours (in a hexagonal geometry)

bool Hit::isNeighbour(Hit b){
	float diff=this->strawID - b.strawID;
	int sl = Detector::sideLength;
	if((this->strawID/sl)%2==0) {
		return (std::abs(diff) <= 1 || std::abs(diff) == sl || std::abs(diff-1) == sl); 	
	} else {
		return (std::abs(diff) <= 1 || std::abs(diff) == sl || std::abs(diff+1) == sl); 	
	}
}

bool Hit::isNeighbour(int a, int b) {
	int diff=a-b;
	int sl = Detector::sideLength;
	if((a/sl)%2==0) {
		return (std::abs(diff) <= 1 || std::abs(diff) == sl || std::abs(diff-1) == sl); 	
	} else {
		return (std::abs(diff) <= 1 || std::abs(diff) == sl || std::abs(diff+1) == sl); 	
	}
}
//Retrieves x,y of a particular straw ID

float Hit::getPosX(int ID) {
	int sl = Detector::sideLength;
	if((ID/sl)%2==0) {
		return (float)(ID - sl * (ID/sl));
	} else {
		return (float)(ID - sl * (ID/sl)) + 0.5;
	}
}

float Hit::getPosY(int ID) {
	int sl = Detector::sideLength;
	return (float)(ID/sl) * sqrt(3)/2;

}

float Hit::getPosX() {
	return getPosX(strawID);	
}

float Hit::getPosY() {
	return getPosY(strawID);
}

//Method to check which straw a position lies in. Returns -1 if the position is not in a straw.
int Hit::pos2ID(float x, float y) {

	float fracX = x - (int) x;
	float fracY = fmod(y,sqrt(3));
	int height = 2*round((y-fracY)/sqrt(3));
	if ((fracX-0.5)*(fracX-0.5) + (fracY-sqrt(3)/2.)*(fracY-sqrt(3)/2.) <= 0.25){
		//	printf("Mid at location (%d, %d), coordinates (%f, %f)\n",(int)x,height,fracX,fracY);
		return (int) x + Detector::sideLength*(height+1); 
	} else if ((fracX-1)*(fracX-1) + fracY*fracY <= 0.25){
		//	printf("Bot right at location (%d, %d), coordinates (%f, %f)\n",(int)x,height,fracX,fracY);
		return 1 + (int) x + Detector::sideLength*height; 
	} else if  (fracX*fracX + fracY*fracY <= 0.25) {
		//	printf("Bot left at location (%d, %d), coordinates (%f, %f)\n",(int)x,height,fracX,fracY);
		return (int) x + Detector::sideLength*height; 
	} else if (fracX*fracX + (fracY-sqrt(3))*(fracY-sqrt(3)) <= 0.25){
		//	printf("Top left at location (%d, %d), coordinates (%f, %f)\n",(int)x,height,fracX,fracY);
		return (int) x + Detector::sideLength*(height+2);
	} else if ((fracX-1)*(fracX-1) + (fracY-sqrt(3))*(fracY-sqrt(3)) <= 0.25){
		//	printf("Top right at location (%d, %d), coordinates (%f, %f)\n",(int)x,height,fracX,fracY);
		return  1 + (int) x + Detector::sideLength*(height+2);
	} else {
		return -1; 
	}
}

