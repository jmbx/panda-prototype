#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <stddef.h>
#include <cmath>
#include <string.h>
#include <algorithm>
#include <array>
#include <vector>
#include <random>
#include <iostream>
#include "Cluster.h"
#include "LinearFit.h"
#include <fstream>
#include <iomanip> 
#include "Hit.h"
#include "Detector.h"
#include "ClusterHandler.h"
#include "GridPrinter.h"
#include "TrackFinder.h"
#include "Timer.h"

#define DATA_TAG 0
#define DATA_FIRST_TAG 1
#define DATA_LAST_TAG 2
#define FINISH_TAG 3

//Global variables
int rank, size;
int numWorkers; 
MPI_Datatype MPI_HIT_TYPE;
Timer timer;

int getCyclicNext(int i, int size){
	return i + 1 == size ? 1 : (i + 1) % size;
}

int getCyclicPrev(int i, int size){
	return i - 1 == 0 ? size - 1 : (i - 1) % size;
}

void writeHitsToFile(std::vector<Cluster> clusters) {
	std::ofstream output;
	output.open ("output/hits" + std::to_string(rank) + ".txt");

	for(int i = 0; i < (int)clusters.size(); i++){
		for(int j = 0; j < (int)clusters[i].hits.size(); j++){
			float x = clusters[i].hits[j].getPosX();
			float y = clusters[i].hits[j].getPosY();
			output << x << " " <<  y << " " <<
				std::fixed << std::setprecision (2) << clusters[i].hits[j].time << " " << i << std::endl;
		}
	}
	output.close();
}

std::vector<Cluster> receiveClusters(int source){
	MPI_Status status;
	int numClusterHits;
	int numClusters;
	MPI_Probe(source, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
	MPI_Get_count(&status, MPI_HIT_TYPE, &numClusterHits);	
	Hit* clusterHits = new Hit[numClusterHits]; //Should probably only allocate one array
	MPI_Recv(clusterHits, numClusterHits, MPI_HIT_TYPE, source, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
	//printf("storage recieved hits from %d\n", source);

	MPI_Probe(source, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
	MPI_Get_count(&status, MPI_INT, &numClusters);	
	int* clusterIndex  = new int[numClusters]; //Should probably only allocate one array
	MPI_Recv(clusterIndex, numClusters, MPI_INT, source, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

	//printf("storage recieved index from %d \n", source);
	std::vector<Cluster> clusters = ClusterHandler::toClusters(clusterHits, numClusterHits,clusterIndex, numClusters);
	delete [] clusterHits;
	delete [] clusterIndex;

	return clusters;
}


void performLinearFits(std::vector<Cluster> clusters){
	for(int i = 0; i < (int)clusters.size(); i++){
		Cluster cl = clusters[i];
		int n = cl.hits.size();
		float *x = new float[n];
		float *y = new float[n];
		for(int j = 0; j < n; j++){
			x[j] = cl.hits[j].getPosX();
			y[j] = cl.hits[j].getPosY();
		}
		//LinearFit::Constants c = LinearFit::fit(x, y, n);
		//printf("Cluster %d, a %f, b %f \n",i,  c.a, c.b);
		delete [] x;
		delete [] y;
	}
}

void masterNode(){
	int target = 1;
	float prevMaxTime = -1;


	//Generate hit data
	std::vector<Hit> hits;

	//Generate hits
	Detector::generateHits(hits);
	//int NH = (int)hits.size();

	if(Detector::verbose)
		printf("Numhits %d\n",(int)hits.size());

	timer.start();
	//Distribute the stacks
	int numStacks = (int)hits.size() / Detector::stackSize;
	int S = Detector::stackSize;
	for(int i = 0; i < numStacks; i++){
		int tag = i == 0 ? DATA_FIRST_TAG : DATA_TAG;
		//int tag = i == 0 ? DATA_FIRST_TAG : (i == numStacks-1) ? DATA_LAST_TAG : DATA_TAG; 
		//int amount = (i == numStacks-1) ? S + (hits.size() % S): S; 
		MPI_Send(&hits.front()+i*S, S, MPI_HIT_TYPE, target, tag, MPI_COMM_WORLD);
		MPI_Send(&prevMaxTime, 1, MPI_FLOAT, target, tag, MPI_COMM_WORLD);
		prevMaxTime = -1;
		for(int j = 0; j < S; j++){
			prevMaxTime = std::max(prevMaxTime, hits[i*S + j].time);	
		}
		//printf("Master node sent stack to %d\n", target);
		target = getCyclicNext(target, size);
	}	
	//Send the last hits (modulo)
	MPI_Send(&hits.front()+(numStacks)*S, (hits.size() % S), MPI_HIT_TYPE, target, DATA_LAST_TAG, MPI_COMM_WORLD);
	MPI_Send(&prevMaxTime, 1, MPI_FLOAT, target, DATA_LAST_TAG, MPI_COMM_WORLD);


	//Send done message
	int number;
	for(int i = 1; i <= numWorkers; i++){
		MPI_Send(&number, 1, MPI_INT, i, FINISH_TAG, MPI_COMM_WORLD);
	}
	std::vector<Cluster> finalClusters;

	//Receieve clusters
	for(int i = 1; i <=numWorkers; i++){
		//printf("Waiting for %d \n", i);
		int num;
		MPI_Status status;
		MPI_Recv(&num, 1, MPI_INT, i, FINISH_TAG, MPI_COMM_WORLD, &status);
		//printf("Waiting for %d \n", i);
		//std::vector<Cluster> clusters = receiveClusters(i);
		//finalClusters.insert(finalClusters.begin(), clusters.begin(), clusters.end());
	}
	if(Detector::verbose)
		timer.stopAndPrint("Total time: ");

	std::vector<Hit> finalHits;
	for (int i = 0; i < (int)finalClusters.size(); i++) {
		//printf("Cluster# %d : ", i);
		for (int j=0; j < (int)finalClusters[i].hits.size(); j++) {
			finalHits.push_back(finalClusters[i].hits[j]);
			//printf("(%f, %d)", finalClusters[i].hits[j].time, finalClusters[i].hits[j].strawID);
		}
		//printf("\n");
	}
	if (Detector::write) {
		GridPrinter::printGrid();
	}

}

void workerNode() {
	MPI_Status status;
	MPI_Status statusMode;
	bool running = true;
	std::vector<Cluster> allClusters;
	Timer tim;
	tim.start();

	TrackFinder tf (rank);
	while(running){

		MPI_Probe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &statusMode);

		if(statusMode.MPI_TAG == DATA_TAG || statusMode.MPI_TAG == DATA_FIRST_TAG || statusMode.MPI_TAG == DATA_LAST_TAG){

			int numHits;
			MPI_Request req[2];		
			float prevMaxTime;

			//Receive one hit data stack
			MPI_Get_count(&statusMode, MPI_HIT_TYPE, &numHits);	
			Hit* hits = new Hit[numHits]; //Should probably only allocate one array
			MPI_Recv(hits, numHits, MPI_HIT_TYPE, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			MPI_Recv(&prevMaxTime, 1, MPI_FLOAT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

			//Perform clustering on the hits
			Timer t;
			t.start();
			std::vector<Cluster> clusters = ClusterHandler::cluster(hits, numHits);
			//if(Detector::verbose)
				//t.stopAndPrint("Cluster time ");

			//Send clusters to previous for edge (in time) hits
			if(statusMode.MPI_TAG != DATA_FIRST_TAG){
				std::vector<Hit> clusterHits;
				std::vector<int> clusterIndex;
				for(int i = 0; i < (int)clusters.size(); i++){
					bool shouldSend = false;
					for(int j = 0; j < (int)clusters[i].hits.size(); j++){
						shouldSend = std::abs(clusters[i].hits[j].time - prevMaxTime) < Detector::epsilon;
					}	
					if(shouldSend){
						clusterHits.insert(clusterHits.end(), clusters[i].hits.begin(), clusters[i].hits.end()); 
						clusterIndex.push_back(clusterHits.size());

						//Remove this cluster from this node
						clusters.erase(clusters.begin() + i);
						i--;
					}
				}
				MPI_Isend(&clusterHits.front(), clusterHits.size(), MPI_HIT_TYPE, getCyclicPrev(rank, size), 
						DATA_TAG, MPI_COMM_WORLD, &req[0]); 
				MPI_Isend(&clusterIndex.front(), clusterIndex.size(), MPI_INT, getCyclicPrev(rank, size),
						DATA_TAG, MPI_COMM_WORLD, &req[1]); 
			} 

			//Receieve edge (in time) clusters from another node
			if(statusMode.MPI_TAG != DATA_LAST_TAG){
				int source = getCyclicNext(rank, size);
				//Receieve clusters from other node
				std::vector<Cluster> newClusters = receiveClusters(source); 

				//Insert the new clusters in the original vector and cluster them
				ClusterHandler::merge(clusters, newClusters);
			}

			//Filter out the clusters with less than 3 hits
			for (int i = 0; i < (int)clusters.size(); i++) {
				if (clusters[i].hits.size()< 3) {
					clusters.erase(clusters.begin()+i);
					i--;
				}
			}		

			tim.start();

			//Perform linear fit
			for(int i = 0; i < (int)clusters.size(); i++) {
				tf.generateTracks(clusters[i].hits);
			}	
			//Wait until all send buffers are done
			if(statusMode.MPI_TAG != DATA_FIRST_TAG){
				MPI_Waitall(2, req, MPI_STATUSES_IGNORE);
			}

			delete [] hits;
			allClusters.insert(allClusters.end(), clusters.begin(), clusters.end());
		}else{

			//The process is finished, send the final clusters to the master node

			int number;
			MPI_Recv(&number, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			//printf("Rank %d finished \n", rank);
			running = false;

			MPI_Send(&number, 1, MPI_INT, 0, FINISH_TAG, MPI_COMM_WORLD);

			if (Detector::write) {
				writeHitsToFile(allClusters); 
			}
			//Send the final clusters to the master node
			/*std::vector<Hit> clusterHits;
			  std::vector<int> clusterIndex;
			  for(int i = 0; i < (int)allClusters.size(); i++){
			  clusterHits.insert(clusterHits.end(), allClusters[i].hits.begin(), allClusters[i].hits.end()); 
			  clusterIndex.push_back(clusterHits.size());
			  }	
			  printf("Rank %d sending clusters to storage with size %d\n", rank, (int)clusterIndex.size());
			  MPI_Ssend(&clusterHits.front(), clusterHits.size(), MPI_HIT_TYPE, 0, DATA_TAG, MPI_COMM_WORLD);
			  MPI_Ssend(&clusterIndex.front(), clusterIndex.size(), MPI_INT, 0, DATA_TAG, MPI_COMM_WORLD);
			  break;	 */	
		}
	}
}

bool isInt(char* line){
	char* p;
	strtol(line, &p, 10);
	return *p == 0;
}
int main(int argc, char* argv[]){

	Detector::verbose = false;
	Detector::write = true;
	//Reading input arguments
	if (argc != 0) {
		for (int i = 0; i < argc; i++) {
			if (strcmp(argv[i], "-v") == 0 || strcmp(argv[i], "-verbose") == 0) {
				Detector::verbose = true;
			}
			if (strcmp(argv[i], "-nw") == 0 || strcmp(argv[i], "-nowrite") == 0) {
				Detector::write = false;
			}
			if ( (strcmp(argv[i], "-nh") == 0 || strcmp(argv[i], "-numhits") == 0) && ( i + 1 < argc && isInt(argv[i+1]))) {
				Detector::numHits = atoi(argv[i+1]);
			}
			if ( (strcmp(argv[i], "-ss") == 0 || strcmp(argv[i], "-stacksize") == 0) && ( i + 1 < argc && isInt(argv[i+1]))) {
				Detector::stackSize = atoi(argv[i+1]);
			}
		}
	}
	//Initializing the node
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	srand(time(NULL));
	numWorkers = size - 1;

	//Creating an MPI datatype for the event (containing INT and FLOAT)
	int blocklengths[2] = {1,1};
	MPI_Datatype types[2] = {MPI_INT, MPI_FLOAT};
	MPI_Aint offsets[2];

	offsets[0] = offsetof(Hit, strawID);
	offsets[1] = offsetof(Hit, time);
	MPI_Type_create_struct(2, blocklengths, offsets, types, &MPI_HIT_TYPE);
	MPI_Type_commit(&MPI_HIT_TYPE);

	if(rank == 0){
		//If rank==0, the node is the root node that sends the events to the other nodes
		masterNode();
	}else{
		//Otherwise, the node is one of the worker nodes
		workerNode();
	}

	MPI_Finalize();	
	if(rank == 0) {

	}

	return 0;
}
