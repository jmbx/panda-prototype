#pragma once
#include <vector>
#include <map>
#include "Cluster.h"
#include <omp.h>
#include <set>
#include <fstream>

typedef struct {
	Hit* hit;
	bool ambigous;
	std::set<int> ids;
} Cell;

class TrackFinder {
	public:
		TrackFinder(int rank);
        ~TrackFinder();
		void generateTracks(std::vector<Hit> &hits);

	private:
		int rank;
		std::vector<Hit> hits;
		Cell *cells;
		Cell* buffer;
		std::map<int, std::set<int>> idMap; //Automata id to strawID set
		std::set<int> testedClusters;
		std::set<std::pair<int,int>> testedPairs;
		std::ofstream dpFile;
		std::ofstream lfFile;

		void performAutomata();
		void findAmbigousDisplacedVertices();
		void findTrackletDisplacedVertices();
		std::vector<Hit*> getNeighbours(Hit &h, std::vector<Hit> &hits);
		std::vector<Hit*> getNeighbours(int a, std::vector<Hit> &hits);
};
