#pragma once
#include <chrono>
#include <string>

using namespace std::chrono;

class Timer {
	public:
		void start();
		void stop();
		void stopAndPrint();
		void stopAndPrint(std::string init);
		//float getTime();
		void print();
	private:
		high_resolution_clock::time_point t1;	
		high_resolution_clock::time_point t2;	
};
