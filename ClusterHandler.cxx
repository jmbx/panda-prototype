#include "ClusterHandler.h"
#include <algorithm>
#include <iostream>
#include "Detector.h"

using std::vector;

bool sortByTime(Hit *a, Hit *b) {
	return a->time < b->time;
}

void ClusterHandler::sortHits(std::vector<Hit> hits, int numHits) {
	std::vector<Hit*> sorted;
	std::sort(sorted.begin(), sorted.end(), sortByTime);	
}

struct HitBin {
	float minTime;
	float maxTime;
	vector<Hit> hits;
};

std::vector<Cluster> ClusterHandler::cluster(Hit *hits, int numHits) {
		vector<Cluster> allClusters;
		vector<HitBin> bins;

		for(int i = 0; i < numHits; i++) {
			float t = hits[i].time;	
			bool added = false;
			for(int j = 0; j < (int)bins.size(); j++){
				if(t > bins[j].maxTime - Detector::epsilon && 
						t < bins[j].minTime + Detector::epsilon) {
					bins[j].hits.push_back(hits[i]);	
					bins[j].minTime = std::min(bins[j].minTime, t);
					bins[j].maxTime = std::max(bins[j].maxTime, t);
					added = true;
					break;
				}
			}	
			if(!added) {
				HitBin bin;
				bin.hits.push_back(hits[i]);
				bin.minTime = t;
				bin.maxTime = t;
				bins.push_back(bin);	
			}

		}

//#pragma omp parallel for
		for(int i = 0; i < (int)bins.size(); i++) {
			vector<Cluster> clusters = clusterHits(bins[i].hits);
//#pragma omp critical
			allClusters.insert(allClusters.end(), clusters.begin(), clusters.end());
		}
		return allClusters;
}

//Merges two clusters and sets the result to a
//Assumes that a and b are already clustered
void ClusterHandler::merge(std::vector<Cluster> &a, std::vector<Cluster> &b) {
	for(int i = 0; i < (int)b.size(); i++){
		for(int j = 0; j < (int)a.size(); j++) {
			if(b[i].canJoin(a[j])) {
				//Check if min,max time should be updated
				if(a[j].minTime < b[i].minTime){
					b[i].minTime = a[j].minTime;	
				}
				if(a[j].maxTime > b[i].maxTime){
					b[i].maxTime = a[j].maxTime;	
				}

				a[j].hits.insert(a[j].hits.end(), b[i].hits.begin(), b[i].hits.end()); 
				//clusters.erase(clusters.begin() + i);
			}
		}		
	}
}

//Method to merge a series of hits to a cluster
std::vector<Cluster> ClusterHandler::clusterHits(std::vector<Hit> &hits){

	std::vector<Cluster> clusters;
	for(int i = 0; i < (int)hits.size(); i++){
		Cluster cl;
		cl.minTime = hits[i].time;
		cl.maxTime = hits[i].time;
		cl.hits.push_back(hits[i]);
		clusters.push_back(cl);
	}
	cluster(clusters);
	return clusters;
}

void ClusterHandler::cluster(std::vector<Cluster> &clusters){
	bool changed;
merged: changed = true;
		while(changed){
			changed = false;
			for(int i = 0; i < (int)clusters.size(); i++){
				for(int j = 0; j < (int)clusters.size(); j++){
					if(i!=j && clusters[i].canJoin(clusters[j])){

						//Check if min,max time should be updated
						if(clusters[j].minTime < clusters[i].minTime){
							clusters[i].minTime = clusters[j].minTime;	
						}
						if(clusters[j].maxTime > clusters[i].maxTime){
							clusters[i].maxTime = clusters[j].maxTime;	
						}

						clusters[i].hits.insert(clusters[i].hits.end(), clusters[j].hits.begin(), clusters[j].hits.end()); 
						clusters.erase(clusters.begin() + j);
						goto merged;	
					}
				}	
			}	
		}
		//return clusters;
}

std::vector<Cluster> ClusterHandler::toClusters(Hit* clusterHits, int numHits, int* clusterIndex, int numClusters){
	std::vector<Cluster> newClusters;
	int start = 0;
	for(int j = 0; j < numClusters; j++){
		Cluster cl;
		std::vector<Hit> newHits;

		for(int k = start; k < clusterIndex[j]; k++){
			Hit h;
			h.time = clusterHits[k].time;
			h.strawID = clusterHits[k].strawID;
			newHits.push_back(h);	
		}
		cl.hits.insert(cl.hits.begin(), newHits.begin(), newHits.end());
		newClusters.push_back(cl);	
		start = clusterIndex[j];
	}
	return newClusters;
}

void ClusterHandler::toAlignedClusters(std::vector<Cluster> clusters, std::vector<Hit> clusterHits, std::vector<int> clusterIndex) {

	for(int i = 0; i < (int)clusters.size(); i++){
		clusterHits.insert(clusterHits.end(), clusters[i].hits.begin(), clusters[i].hits.end()); 
		clusterIndex.push_back(clusterHits.size());
	}	
}

