#include "LinearFit.h"


std::tuple<float,float> LinearFit::fit(float *x, float *y, int n){
	double xsum=0,x2sum=0,ysum=0,xysum=0;
	for (int i = 0; i < n; i++){
		xsum = xsum+x[i];
		ysum = ysum+y[i];
		x2sum = x2sum+x[i]*x[i];;
		xysum = xysum+x[i]*y[i];
	}
	std::tuple<float,float> c( (n*xysum-xsum*ysum)/(n*x2sum-xsum*xsum), 
			(x2sum*ysum-xsum*xysum)/(x2sum*n-xsum*xsum));
	return c;
} 

float LinearFit::variance(float *x, int n) {
	double var = 0, mean = 0;
	for (int i = 0; i < n; i++) {
		mean += x[i];
	}
	mean /= n;
	for (int i = 0; i < n; i++) {
		var += (x[i] - mean) * (x[i] - mean);
	}
	var /= n;
	return var;
	}

float LinearFit::meanSqError(float *x, float* y, int n, std::tuple<float, float> c) {
	float dev = 0;
	float a = std::get<0>(c);
	float b = std::get<1>(c);
	for(int i = 0; i < n; i++) {
		dev += (b + a*x[i] - y[i])*(b + a*x[i] - y[i]);
	}	

	return std::sqrt(1.0 / (n-1) * dev);
}
