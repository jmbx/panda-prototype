#include <vector>
#include "Cluster.h"
#include "Hit.h"
#include <cmath>
#include "Detector.h"

bool Cluster::canJoin(Cluster b){

	if(b.minTime - this->maxTime > Detector::epsilon ||
	  this->minTime - b.maxTime > Detector::epsilon ||
	  std::max(this->maxTime, b.maxTime) - std::min(this->minTime, b.minTime) > Detector::epsilon) {
		return false;
	}

	for (int i=0; i < (int)this->hits.size(); i++) {
		for(int j=0; j < (int)b.hits.size(); j++) {
			if (this->hits[i].isNeighbour(b.hits[j])) {
				return true;
			}
		}
	}

	return false;
}


