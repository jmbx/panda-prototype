#pragma once
#include "Hit.h"
#include <vector>

class Detector {
	public:
		static void generateRandHits(std::vector<Hit> &hits, float t0, float tEnd, int numHits);
		static void generateLineHits(Hit* hits);
		static void generateHits(std::vector<Hit> &hits);
		static void generateLineHits(std::vector<Hit> &hits, float angle, float xcenter, float ycenter, float t0, int dontAddID);
		static void generateV(std::vector<Hit> &hits, float directionAngle, float separationAngle, float xcenter, float ycenter, float t0);

		static const int sideLength;
		static const float epsilon;
		static const float sigma;
		static int stackSize;
		static int numHits;
		static bool write;
		static bool verbose;
}; 


