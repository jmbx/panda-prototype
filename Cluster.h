#pragma once

#include "Hit.h"
#include <vector>

class Cluster {
	public:
		bool canJoin(Cluster b);
		std::vector<Hit> hits;
		float minTime;
		float maxTime;
};
