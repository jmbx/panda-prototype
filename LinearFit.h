#pragma once

#include "Cluster.h"
#include <iostream>
#include <cmath>
#include <tuple>

class LinearFit {
	public:
		static std::tuple<float,float> fit(float *x, float*y, int n);
		static float meanSqError(float*x, float* y, int n, std::tuple<float,float> c);
		static float variance(float *x, int n);
};
