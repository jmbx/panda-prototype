
CC=mpic++
FLAGS=-Wall -g -std=c++11 -O3 -fopenmp
LFLAGS=-fopenmp

OBJECTS=main.o Cluster.o Detector.o Hit.o TrackFinder.o LinearFit.o ClusterHandler.o GridPrinter.o Timer.o

main: $(OBJECTS)
	$(CC) $(OBJECTS) $(LFLAGS) -o main
	
main.o: main.cxx
	$(CC) main.cxx -c $(FLAGS) 

Cluster.o: Cluster.cxx Cluster.h
	$(CC) Cluster.cxx -c $(FLAGS) 

Hit.o: Hit.cxx Hit.h 
	$(CC) Hit.cxx -c $(FLAGS)

Detector.o: Detector.cxx Detector.h
	$(CC) Detector.cxx -c $(FLAGS) 

TrackFinder.o: TrackFinder.cxx TrackFinder.h
	$(CC) TrackFinder.cxx -c $(FLAGS)

LinearFit.o: LinearFit.cxx LinearFit.h
	$(CC) LinearFit.cxx -c $(FLAGS)

ClusterHandler.o: ClusterHandler.cxx ClusterHandler.h
	$(CC) ClusterHandler.cxx -c $(FLAGS)

GridPrinter.o: GridPrinter.cxx GridPrinter.h
	$(CC) GridPrinter.cxx -c $(FLAGS)

Timer.o: Timer.cxx Timer.h
	$(CC) Timer.cxx -c $(FLAGS)

test: test.cxx
	g++ -std=c++11 -O3 -g -fopenmp test.cxx Cluster.cxx Detector.cxx TrackFinder.cxx Hit.cxx ClusterHandler.cxx LinearFit.cxx GridPrinter.cxx 

clean: 
	rm -f *.o
	rm -f main
	rm -f *.txt
	rm -f output/*.txt
