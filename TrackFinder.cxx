#include "TrackFinder.h"
#include <vector>
#include "Detector.h"
#include <cmath>
#include <algorithm>
#include <iostream>
#include <set>
#include <omp.h>
#include "LinearFit.h"
#include <fstream>


TrackFinder::TrackFinder(int nodeRank) {
	rank = nodeRank;
    if(Detector::write) {
        dpFile.open ("output/displaced" + std::to_string(rank) + ".txt");
        lfFile.open ("output/linearFits" + std::to_string(rank) + ".txt");
    }

}

TrackFinder::~TrackFinder() {
    if(Detector::write) {
        dpFile.close();
        lfFile.close();
    }
}


//Method to obtain the neighbours of a hit h, returned in a vector

std::vector<Hit*> TrackFinder::getNeighbours(Hit &h, std::vector<Hit> &hits) {
    std::vector<Hit*> nbs;
    std::set<int> nbIDs;
    for(int i = 0; i < (int)hits.size(); i++){
        if(hits[i].isNeighbour(h) && hits[i].strawID != h.strawID && nbIDs.find(hits[i].strawID) == nbIDs.end()){
            nbIDs.insert(hits[i].strawID);
            nbs.push_back(&hits[i]);
        }
    }
    return nbs;
}

std::vector<Hit*> TrackFinder::getNeighbours(int a, std::vector<Hit> &hits) {
    std::vector<Hit*> nbs;
    std::set<int> nbIDs;
    for(int i = 0; i < (int)hits.size(); i++){
        if(Hit::isNeighbour(a,hits[i].strawID) && hits[i].strawID != a && nbIDs.find(hits[i].strawID) == nbIDs.end()){
            nbIDs.insert(hits[i].strawID);
            nbs.push_back(&hits[i]);
        }
    }
    return nbs;
}

//Returns the the index of the hit h in the array of cells, -1 if not contained
int getID(Hit *h, Cell* cells, int length) {
    for(int i = 0; i < length; i++) {
        if(cells[i].hit == h) {
            return i;
        }		
    }
    return -1;
} 

//Performs Jette Schumanns automata for hit grouping

void TrackFinder::performAutomata() {
    //Set all cells to its strawID
    //#pragma omp parallel for
    for(int i = 0; i < (int)hits.size(); i++) {
        Hit h = hits[i];
        Cell c;
        c.hit = &hits[i];
        std::vector<Hit*> nbs = getNeighbours(hits[i], hits);
        int numNeighbours = 0;
        for(int k = 0; k < (int)nbs.size(); k++) {
            if(nbs[k]->strawID != h.strawID) {
                numNeighbours++;	
            }
        }
        c.ambigous = numNeighbours > 2;
        if(!c.ambigous) {
            c.ids.insert(h.strawID);
        }
        cells[i] = c;
        buffer[i] = c;
    }

    for(int i = 0; i < (int)hits.size(); i++) {
        std::vector<Hit*> nbs = getNeighbours(hits[i], hits);
        if(!buffer[i].ambigous) {
            bool shouldBe = true;
            for(int k = 0; k < (int)nbs.size(); k++) {
                Hit* nb = nbs[k];
                if(!buffer[getID(nb, buffer, hits.size())].ambigous){
                    shouldBe = false;
                }	
            }
            cells[i].ambigous = shouldBe;
            if(shouldBe)
                cells[i].ids.clear();
        } 
    }
    for(int i = 0; i < (int)hits.size(); i++) {
        buffer[i] = cells[i];
    }

    //Set all non ambigious cell to min of neighbours
    bool changed = true;
    while(changed) {
        changed = false;
        //#pragma omp parallel for
        for(int i = 0; i < (int)hits.size(); i++) {
            bool didChange = false;
            Cell c = buffer[i];
            Hit *h = c.hit;
            if(!c.ambigous) {
                std::vector<Hit*> nbs = getNeighbours(*h, hits);
                int minID = *c.ids.begin();
                for(int i = 0; i < (int)nbs.size(); i++){
                    Cell c2 = cells[getID(nbs[i], buffer, hits.size())];
                    int newMin = std::min(minID, *c2.ids.begin());
                    if(!c2.ambigous && newMin != minID) {
                        minID = newMin;
                        didChange = true;	
                        c.ids.clear();
                        c.ids.insert(minID);
                    }
                }
                if(didChange) {
                    //#pragma omp critical
                    changed = true;
                }
                cells[i] = c;
            }

        }
        //#pragma omp parallel for
        for(int i = 0; i < (int)hits.size(); i++) {
            buffer[i] = cells[i];
        }
    }

    //Find ambiguities
    changed = true;
    while(changed) {
        changed = false;
        //#pragma omp parallel for
        for(int i = 0; i < (int)hits.size(); i++) {
            bool didChange = false;
            Cell c = buffer[i];
            Hit *h = c.hit;
            if(c.ambigous) {
                std::vector<Hit*> nbs = getNeighbours(*h, hits);
                for(int i = 0; i < (int)nbs.size(); i++){
                    Cell c2 = cells[getID(nbs[i], buffer, hits.size())];
                    int sprev = c.ids.size();
                    c.ids.insert(c2.ids.begin(), c2.ids.end());	
                    int snew = c.ids.size();
                    if(sprev != snew) {
                        didChange = true;
                    }
                }

                if(didChange) {
                    //#pragma omp critical
                    changed = true;
                }
                cells[i] = c;
            }
        }
        //#pragma omp parallel for
        for(int i = 0; i < (int)hits.size(); i++) {
            buffer[i] = cells[i];
        }
    }


}

// Method for detecting displaced vertices using intersection of linefits
void TrackFinder::findAmbigousDisplacedVertices() {
    //Find permutations for ambigous cells
    //#pragma omp parallel for
    for(int i = 0; i < (int)hits.size(); i++) {
        Cell c = cells[i];
        if(cells[i].ambigous){

            //If two ids, we are likely to have a displaced vertex
            if(c.ids.size() == 2) {

                auto idIterator = c.ids.begin();
                int firstID = *idIterator;
                int secondID = *(++idIterator);

                bool shouldContinue = false;
                //#pragma omp critical
                {
                    shouldContinue = (testedPairs.find(std::make_pair(firstID, secondID)) != testedPairs.end() || 
                            testedPairs.find(std::make_pair(secondID, firstID)) != testedPairs.end());
                    testedPairs.insert(std::make_pair(firstID, secondID));
                    testedClusters.insert(firstID);
                    testedClusters.insert(secondID);
                }

                if(shouldContinue) {
                    continue;
                }
                auto strawIds1 = idMap[firstID];
                auto strawIds2 = idMap[secondID];

                std::vector<float> xvals1, yvals1;
                std::vector<float> xvals2, yvals2;
                float xmin = 100, xmax = 0;

                //Perform linear fit for the first id
                for(auto strawIt = strawIds1.begin(); strawIt != strawIds1.end(); strawIt++) {
                    xvals1.push_back(Hit::getPosX(*strawIt));	
                    yvals1.push_back(Hit::getPosY(*strawIt));	
                    xmin = std::min(xmin, Hit::getPosX(*strawIt));
                    xmax = std::max(xmax, Hit::getPosX(*strawIt));
                }
                std::tuple<float,float> cs1 = LinearFit::fit(&xvals1.front(), &yvals1.front(), xvals1.size());
                //float dev1 = LinearFit::meanSqError(&xvals1.front(), &yvals1.front(), xvals1.size(), cs1);
                if(Detector::write) {
                    lfFile << xmin << " " << std::get<1>(cs1) + std::get<0>(cs1) * xmin << std::endl; 
                    lfFile << xmax << " " << std::get<1>(cs1) + std::get<0>(cs1) * xmax << std::endl << std::endl;
                }
                xmin = 100, xmax = 0;

                //Perform linear fit for the second id
                for(auto strawIt = strawIds2.begin(); strawIt != strawIds2.end(); strawIt++) {
                    xvals2.push_back(Hit::getPosX(*strawIt));	
                    yvals2.push_back(Hit::getPosY(*strawIt));	
                    xmin = std::min(xmin, Hit::getPosX(*strawIt));
                    xmax = std::max(xmax, Hit::getPosX(*strawIt));
                }
                std::tuple<float,float> cs2 = LinearFit::fit(&xvals2.front(), &yvals2.front(), xvals2.size());
                //float dev2 = LinearFit::meanSqError(&xvals2.front(), &yvals2.front(), xvals2.size(), cs2);
                if(Detector::write) {
                    lfFile << xmin << " " << std::get<1>(cs2) + std::get<0>(cs2) * xmin << std::endl; 
                    lfFile << xmax << " " << std::get<1>(cs2) + std::get<0>(cs2) * xmax << std::endl << std::endl;
                    std::cout << xmax << " " << std::get<1>(cs2) + std::get<0>(cs2) * xmax << std::endl << std::endl;
                }
                float k1 = std::get<0>(cs1);
                float k2 = std::get<0>(cs2);
                float m1 = std::get<1>(cs1);
                float m2 = std::get<1>(cs2);
                float a1 = std::atan(k1);
                float a2 = std::atan(k2);
                if(Detector::verbose) {
                    printf("Slope 1: %f, ID = %d, xvals %d \n", k1, firstID, (int)xvals1.size());
                    printf("Slope 2: %f, ID = %d, xvals %d \n", k2, secondID, (int)xvals2.size());
                }
                if(std::abs(std::atan2(sin(a1-a2), std::cos(a1-a2))) > 0.34) {
                    if(Detector::verbose) {
                        //printf("Found likely displaced vertex\n");
                    }
                    float x = (m2 - m1) / (k1 - k2);

                    if(Detector::write) {
                        dpFile << x << " " << k1*x + m1 << std::endl; 
                    }
                }
            }
        }
    }


}

//Method for detecting displaced vertices in unambigous lines.
//Uses the fact that the vertex maximizes the sum of the distances in a V-shape

void TrackFinder::findTrackletDisplacedVertices() {
    for(auto it = idMap.begin(); it != idMap.end(); it++) {
        if(testedClusters.find(it->first) == testedClusters.end()) {
            //Non ambigous tracklet, perform linear fit	
            std::vector<float> xvals, yvals;
            float xmin = 100, xmax = 0;
            float ymin = 100, ymax = 0;
            std::vector<int> endPoints;
            for(auto c = it->second.begin(); c != it->second.end(); c++) {
                xvals.push_back(Hit::getPosX(*c));	
                yvals.push_back(Hit::getPosY(*c));	
                xmin = std::min(xmin, Hit::getPosX(*c));
                xmax = std::max(xmax, Hit::getPosX(*c));
                ymin = std::min(ymin, Hit::getPosY(*c));
                ymax = std::max(ymax, Hit::getPosY(*c));
                if (getNeighbours(*c, hits).size() == 1) {
                    endPoints.push_back(*c);
                }
            }
            if(xvals.size() > 1) {
                //Calculate variances to handle vertical lines for linear fitting
                float varX = LinearFit::variance(&xvals.front(), xvals.size());
                float varY = LinearFit::variance(&yvals.front(), yvals.size());
                bool inverted = false;
                std::tuple<float,float> cs; 
                float dev;
                if (varY > 1.5*varX) {
                    cs = LinearFit::fit(&yvals.front(), &xvals.front(), yvals.size());
                    dev = LinearFit::meanSqError(&yvals.front(), &xvals.front(), yvals.size(), cs);
                    inverted = true;
                } else {
                    cs = LinearFit::fit(&xvals.front(), &yvals.front(), xvals.size());
                    dev = LinearFit::meanSqError(&xvals.front(), &yvals.front(), xvals.size(), cs);
                }
                //Inverted line: x = cs(0)y+ cs(1), Noninverted: y = x*cs(0) + cs(1)
                if(dev > 0.9) {

                    /*printf("Coordinates: ");
                      for (int i = 0; i < (int)xvals.size(); i++) {
                      printf("(%f, %f) ", xvals.at(i), yvals.at(i));
                      }
                      printf("\n");

                      printf("Index of endPoints: ");
                      for (int i = 0; i < (int)endPoints.size(); i++) {
                      printf("%d ", endPoints[i]);
                      }
                      printf("\n");
                      */
                    if (endPoints.size() != 2) {
                        continue;
                    }
                    float maxDist = 0;
                    int vertexIndex = -1;
                    float x1 = Hit::getPosX(endPoints[0]);
                    float y1 = Hit::getPosY(endPoints[0]);
                    float x2 = Hit::getPosX(endPoints[1]);
                    float y2 = Hit::getPosY(endPoints[1]);

                    /*printf("Endpoint IDs: %d, %d \n",endPoints[0], endPoints[1]);

                      printf("Endpoint coordinates: (%f, %f), (%f, %f)\n",x1,y1,x2,y2);
                      */
                    for (int i = 0; i < (int)xvals.size(); i++) {
                        float dist = std::sqrt((xvals[i]-x1)*(xvals[i]-x1) + (yvals[i]-y1)*(yvals[i]-y1)) 
                            + std::sqrt((xvals[i]-x2)*(xvals[i]-x2) + (yvals[i]-y2)*(yvals[i]-y2)); 

                        if (dist > maxDist) {

                            //printf("New maxDist updated: %f\n", dist);
                            maxDist = dist;
                            vertexIndex = i;
                        }
                    }


                    /*
                       int length = xvals.size();
                       int optCutNode = 0;
                       std::tuple<float,float> cs1, cs2;
                       float dev1, dev2, minDevSum = 10;


                       for (int i = 0; i < length; i++) {
                       cs1 = LinearFit::fit(&xvals.front()+i, &yvals.front()+i,length-i);
                       cs2 = LinearFit::fit(&xvals.front(), &yvals.front(),i);
                       dev1 = LinearFit::meanSqError(&xvals.front()+i, &yvals.front()+i, length-i, cs1);
                       dev2 = LinearFit::meanSqError(&xvals.front(), &yvals.front(), i, cs2);

                       if (dev1+dev2 < minDevSum) {
                       minDevSum = dev1 + dev2;
                       optCutNode = i;
                       }
                       } */						

                    if(Detector::verbose) {
                        //printf("Dev %f, found likely displaced vertex (unambigous type) with ID %d, dist %f\n", dev, vertexIndex, maxDist);
                    }
                    if(Detector::write) {
                        dpFile << xvals.at(vertexIndex) << " " << yvals.at(vertexIndex) << std::endl;	
                    }
                }
                if(Detector::write) {
                    if (inverted) {
                        lfFile << std::get<1>(cs) + std::get<0>(cs) * ymin << " " << ymin << std::endl; 
                        lfFile << std::get<1>(cs) + std::get<0>(cs) * ymax << " " << ymax << std::endl << std::endl; 
                    } else {
                        lfFile << xmin << " " << std::get<1>(cs) + std::get<0>(cs) * xmin << std::endl; 
                        lfFile << xmax << " " << std::get<1>(cs) + std::get<0>(cs) * xmax << std::endl << std::endl; 
                    }
                }
            }
        }
    } 	
}

//Calculates the tracks in parallel using openmp
void TrackFinder::generateTracks(std::vector<Hit> &inHits) {

    //Set number of openmp threads to 4
    omp_set_num_threads(1);
    hits = inHits;

    cells = new Cell[hits.size()];
    buffer = new Cell[hits.size()];

    //perform Jette Schumanns cellular automaton
    performAutomata();

    //Compute the idMap
    for(int i = 0; i < (int)hits.size(); i++) {
        if(!cells[i].ambigous){
            idMap[*cells[i].ids.begin()].insert(cells[i].hit->strawID);	
        }
    }

    //Find displaced vertices
    findAmbigousDisplacedVertices();
    findTrackletDisplacedVertices();
    delete [] buffer;
    delete [] cells;
}
