#!/bin/bash


NODES=${1:-4}

make
mpirun -np ${NODES} ./main
gnuplot -p plotter.p
