#pragma once

class Hit {
	public:
		int strawID;
		float time;
		bool isNeighbour(Hit b);
		static bool isNeighbour(int a, int b);
		static float getPosX(int ID);
		static float getPosY(int ID);
		float getPosX();
		float getPosY();
		static int pos2ID(float x, float y);
};
